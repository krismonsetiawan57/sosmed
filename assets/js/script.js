$(document).ready(function () {
    $('#showPass').click(function () {
        var password = $('#password').attr('type');
        if (password === 'password') {
            $('#password').attr('type', 'text');
            $('.custom-control-label').html('Hide Password');
        } else {
            $('#password').attr('type', 'password');
            $('.custom-control-label').html('Show Password');
        }
    });
});