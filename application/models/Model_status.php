<?php
date_default_timezone_set('Asia/Jakarta');
class Model_status extends CI_Model {
    public function tampilStatus($id)
    {
        $this->db->select('*');
        $this->db->join('login','login.id = status.id_user');
        // $this->db->join('comment','comment.id_status = status.id_status');
        $this->db->where('id_user', $id);
        $this->db->order_by('tanggal','DESC');
        return $this->db->get('status');
    }
    public function posting()
    {
        $date = date('Y-m-d H:i:s');
        $data = [
            "id_user" => $this->session->userdata('id'),
            "isi_status" => $this->input->post('status',true),
            "tanggal" => $date,
        ];
        $this->db->insert("status",$data);
    }
    public function tampilComment($id)
    {
        $this->db->select('*');
        $this->db->where('id_status', $id);
        $this->db->order_by('id_comment','DESC');
        return $this->db->get('comment');
    }
    public function addComment()
    {
        $date = date('Y-m-d H:i:s');
        $data = [
            "id_status" => $this->input->post('id_status',true),
            "isi_comment" => $this->input->post('comment',true),
            "tgl" => $date,
        ];
        $this->db->insert("comment",$data);
    }
}