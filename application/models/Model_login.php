<?php
class Model_login extends CI_Model {
    public function getUserById($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get('login');
		return $query;
    }
    public function tambahUsers()
	{
		$data = [
			"username" => $this->input->post('username',true),
			"password" => password_hash($this->input->post('password',true),PASSWORD_DEFAULT),
			"status" => $this->input->post('status',true),
            "last_aktive" => $this->input->post('last_aktive',true),
            "nama" => $this->input->post('nama',true),
            "alamat" => $this->input->post('alamat',true),
		];
		$this->db->insert('login',$data);
    }
    public function masuk()
	{
		$data = [
			"username" => $this->input->post('username',true),
			"password" => $this->input->post('password',true)
		];
		$this->db->select('*');
		$this->db->where('username',$data['username']);
		$query = $this->db->get('login');
		if($query->num_rows() == 1)
		{
			$rows = $query->row();
			if( password_verify($data['password'],$rows->password) )
			{
				$sess = [
					"id" => $rows->id,
					"username" => $rows->username		
				];
				$this->session->set_userdata($sess);
			}else
			{
				$this->session->set_flashdata('info', '<div class="alert alert-danger" role="alert">Username atau Password salah!</div>');
				redirect(base_url('login'));
			}
		}
    }
    public function updateLastAktif($id)
	{
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y-m-d H:i:s');
		$status = "offline";
		$this->db->set('status', $status);
		$this->db->set('last_aktive',$date);
		$this->db->where('id',$id);
		$this->db->update('login');
	}
}