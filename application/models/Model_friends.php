<?php
class Model_friends extends CI_Model {
    public function friends($id)
    {
        $this->db->select('*');
        $this->db->where('id_akun',$id);
        $this->db->join('login','login.id = teman.id_teman');
        $query = $this->db->get('teman');
        return $query;
    }
    public function cekTeman($id,$idteman)
    {
        $this->db->select('*');
        $this->db->where('id_akun',$id);
        $this->db->where('id_akun',$idteman);
        $query = $this->db->get('teman');
        return $query;
    }
    public function saranTeman($id)
	{
		$this->db->select('*');
        $this->db->where('id !=',$id);
		$query = $this->db->get('login');
		return $query;
    }
    public function tambahTeman()
    {
        $data =  [
            "id_akun" => $this->session->userdata('id'),
            "id_teman" => $this->uri->segment(3),
        ];
        $this->db->insert('teman',$data);
        $data2 =  [
            "id_akun" =>  $this->uri->segment(3),
            "id_teman" => $this->session->userdata('id'),
        ];
        $this->db->insert('teman',$data2);
    }
    public function unfollowTeman($id_akun,$id_teman)
    {
        $this->db->where('id_akun', $id_akun);
        $this->db->where('id_teman',$id_teman);
        $this->db->delete('teman');
    }
}