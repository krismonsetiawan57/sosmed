<?php
class Status extends CI_Controller {
    function __construct(){
		parent::__construct();
		$this->load->model('Model_status');
	}
    public function newPost()
    {
        $this->Model_status->posting();
        redirect('beranda');
    }
    public function comment()
    {
        $this->Model_status->addComment();
        redirect(base_url('beranda'));
    }
}