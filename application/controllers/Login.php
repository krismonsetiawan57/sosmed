<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Model_login');
	}
	public function index()
	{
		if(!$this->session->userdata('id'))
		{
			$this->form_validation->set_rules('username', 'Username','required');
			$this->form_validation->set_rules('password', 'Password','required');
			if($this->form_validation->run() == FALSE)
			{
				$data['judul'] = "Halaman Login";
				$this->load->view('template/header',$data);
				$this->load->view('login/index',$data);
				$this->load->view('template/footer');
			}else
			{
				$this->Model_login->masuk();
				redirect(base_url('beranda'));
			}
		}else{
			redirect(base_url('beranda'));
		}
	}
	public function signup()
	{
		if(!$this->session->userdata('id'))
		{
			$this->form_validation->set_rules('username', 'Username','required');
			$this->form_validation->set_rules('password', 'Password','required');
			$this->form_validation->set_rules('nama', 'Nama','required');
			$this->form_validation->set_rules('alamat', 'Alamat','required');
			$this->form_validation->set_rules('password2', 'Repeat Password','required|matches[password]');
			if ($this->form_validation->run() == FALSE)
			{
				$data['judul'] = "Halaman Sign Up";
				$this->load->view('template/header',$data);
				$this->load->view('login/register',$data);
				$this->load->view('template/footer');
			}
			else
			{
				$this->Model_login->tambahUsers();
				redirect('login');
			}
		}else
		{
			redirect(base_url('beranda'));
		}
	}
	public function complete()
	{
		if(!$this->session->userdata('id'))
		{
			$this->form_validation->set_rules('nama', 'Nama','required');
			$this->form_validation->set_rules('alamat', 'Alamat','required');
			$this->form_validation->set_rules('nohp', 'nohp','required|numeric');
			if($this->form_validation->run() == FALSE)
			{
				$data['id'] = $this->session->userdata("id");
				$data['judul'] = "Complete your registration";
				$this->load->view('template/header',$data);
				$this->load->view('login/complete-register',$data);
				$this->load->view('template/footer');
			}else
			{
				$this->Model_users->insertNewUsers();
			}
		}else
		{
			redirect(base_url('beranda'));
		}
	}
	public function logout()
	{
		$id = $this->session->userdata('id');
		$this->Model_login->updateLastAktif($id);
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
