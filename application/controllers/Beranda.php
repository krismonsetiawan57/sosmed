<?php
class Beranda extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_login');
        $this->load->model('Model_friends');
        $this->load->model('Model_status');
    }
    public function index()
    {
        $id = $this->session->userdata('id');
        if(!empty($id))
        {
            $data['id'] = $id;
            $data['judul'] = "Beranda";
            $data['listTeman'] = $this->Model_friends->friends($id)->result_array();
            $query = $this->Model_friends->friends($id)->result_array();
            $data1 = $this->Model_status->tampilStatus($id)->result_array();
            foreach ($query as $rows)
            {
                $id_teman = $rows['id_teman'];
                $data2 = $this->Model_status->tampilStatus($id_teman)->result_array();
                $data1 = array_merge($data1,$data2);
            }
            foreach ($data1 as $cek) {
                $komen = $this->Model_status->tampilComment($cek['id_status'])->num_rows() ;
               if ($komen>= 1) {
                   $data_comment = $this->Model_status->tampilComment($cek['id_status'])->result_array();
               }
            }
            $data['status'] = $data1;
            $data['comment'] = $data_comment;
            $data['profile'] = $this->Model_login->getUserById($id)->row();
            $data['teman'] = $this->Model_friends->saranTeman($id)->result_array();
            $this->load->view('template/header',$data);
            $this->load->view('beranda/index',$data);
            $this->load->view('template/footer');
        }else
        {
            redirect(base_url());
        }
    }
}