<?php
class Friends extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_friends');
    }
    public function add()
    {  
       $this->Model_friends->tambahTeman();
       redirect(base_url('beranda'));
    }
    public function unfollow()
    {
        $id = $this->session->userdata('id');
        $idTeman = $this->uri->segment(3);
        $this->Model_friends->unfollowTeman($id,$idTeman);
        $this->Model_friends->unfollowTeman($idTeman,$id);
        redirect(base_url('beranda'));
    }
}