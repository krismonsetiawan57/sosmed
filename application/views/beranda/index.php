<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 mt-3">
            <h2 class="text-center">Profile</h2>
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title"><?=$profile->nama?></h5>
                    <p class="card-text">
                        <i class="fa fa-map-marker text-danger"></i> <?=$profile->alamat;?>
                    </p>
                    <a href="#" class="card-link">Logout</a>
                </div>
            </div>
            <h2 class="text-center mt-3">Daftar Teman</h2>
            <ul class="list-group">
                <?php
                    foreach ($listTeman as $rows)
                    {
                        ?>
                            <li class="list-group-item">
                                <?=$rows['nama']?>
                                <a href="<?=base_url()?>friends/unfollow/<?=$rows['id_teman']?>" class="float-right"><i class="fa fa-user-times"></i></a>
                            </li>
                        <?php
                    }
                ?>
            </ul>
        </div>
        <div class="col-sm-6 mt-3">
            <div class="row">
                <div class="col-12">
                        <form action="<?=base_url()?>status/newpost" method="POST">
                            <h2 class="text-center">Update Status</h2>
                            <textarea class="form-control" name="status" id="status" cols="30" rows="10" placeholder="Apa yang anda pikirkan?"></textarea>
                            <div class="form-group mt-1">
                                <input type="submit" name="btnKirim" class="btn btn-primary float-right" value="Kirim">
                            </div>
                        </form>
                </div>
           </div>
           <div class="row mt-3">
                <div class="col-12">
                    <?php
                        foreach ($status as $rows) {
                            ?>
                                <div class="card mt-2">
                                <h5 class="card-header"><?=$rows['nama']?></h5>
                                <div class="card-body">
                                    <h5 class="card-title"><?=$rows['tanggal']?></h5>
                                    <p class="card-text"><?=$rows['isi_status']?></p>
                                     <h6 class="ml-3">Komentar</h6>
                                    <?php
                                    
                                        foreach ($comment as $komen) {
                                            if($rows['id_status'] == $komen['id_status']){
                                                ?>
                                                   
                                                    <p class="ml-3"><?=$komen['isi_comment']?></p>
                                                <?php
                                            }
                                        }
                                    ?>
                                    <form action="<?=base_url()?>status/comment" class="form-inline ml-3" method="post">
                                        <input type="hidden" name="id_status" value="<?=$rows['id_status']?>">
                                        <textarea name="comment" id="" cols="30" rows="1" class="form-control mr-3"></textarea>
                                        <input type="submit" value="Komen" class="btn btn-info">
                                    </form>
                                </div>
                                </div>
                            <?php
                        }
                    ?>
                </div>
           </div>
        </div>
        <div class="col-sm-3 mt-3">
            <h2 class="text-center">Teman</h2>
            <ul class="list-group">
                <?php
                    foreach ($teman as $rows) 
                    {
                        ?>
                            <li class="list-group-item">
                                <?=$rows['nama'];?>
                                <a href="<?=base_url()?>friends/add/<?=$rows['id']?>" class="float-right"><i class="fa fa-user-plus"></i></a>
                            </li>
                        <?php
                    }
                ?>
            </ul>
        </div>
    </div>
</div>