<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.css">
    <title><?=$judul?></title>
  </head>
  <body>
      <?php
            if(!empty($id))
            {
                ?>
                <nav class="navbar navbar-expand-lg navbar-light bg-primary sticky-top">
                    <a class="navbar-brand" href="#">SOSMED</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?=base_url()?>">Beranda <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="#">Teman</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">You Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Pemberitahuan</a>
                            </li>
                        </ul>
                        <a href="<?=base_url()?>login/logout" class="btn btn-outline-dark float-right">Logout</a>
                    </div>
                </nav>
                <?php
            }
      ?>