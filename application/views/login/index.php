<div class="login-body">
    <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign In</h5>
            <form class="form-signin" method="post" action="<?=base_url()?>login">
              <?php
                  $error = $this->session->flashdata('info');
                  if (!empty($error)) {
                      echo $error;
                  }
              ?>
              <div class="form-label-group">
                <input type="text" id="username" class="form-control" placeholder="Your Username" name="username" autofocus>
                <label for="username">Username</label>
              </div>
              <div class="form-label-group">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                <label for="password">Password</label>
              </div>

              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="showPass">
                <label class="custom-control-label" for="showPass">Show Passwrod</label>
              </div>
              <?php
                  if(validation_errors())
                  {
                      ?>
                        <div class="alert alert-danger mt-2" role="alert">
                          <?= validation_errors();?>
                        </div>
                      <?php
                  }
              ?>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
              <hr class="my-4">
              <p>Don't have accouts? <a href="<?=base_url()?>login/signup" class="badge badge-danger">SIGN UP</a> here</p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>