<div class="login-body">
    <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign Up</h5>
            <form class="form-signin" action="<?=base_url()?>login/signup" method="post">
              <div class="form-label-group">
                <input type="text" id="username" name="username" class="form-control" placeholder="Your Username" autofocus>
                <label for="username">Username</label>
              </div>
              <div class="form-label-group">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                <label for="password">Password</label>
              </div>
              <div class="form-label-group">
                <input type="password" id="password2" name="password2" class="form-control" placeholder="Repeat Password">
                <label for="password2">Repeat Password</label>
              </div>
              <div class="form-label-group">
                <input type="text" id="nama" name="nama" class="form-control" placeholder="Your Name" autofocus>
                <label for="nama">Nama</label>
              </div>
              <div class="form-label-group">
                <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="3"></textarea>
              </div>
              <input type="hidden" name="status" value="offline">
              <input type="hidden" name="last_aktive" value="<?= date("Y-m-d H:i:s");?>">
              <?php 
                if(validation_errors()){
                    ?>
                    <div class="alert alert-danger mt-2" role="alert">
                        <?= validation_errors();?>
                    </div>
                    <?php
                }?>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign Up</button>
              <hr class="my-4">
              <p>have an accouts? <a href="<?=base_url()?>" class="badge badge-primary">SIGN IN</a> here</p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>