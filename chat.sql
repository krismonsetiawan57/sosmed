-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 13 Mar 2019 pada 14.22
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `comment`
--

CREATE TABLE `comment` (
  `id_comment` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `isi_comment` text NOT NULL,
  `tgl` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `comment`
--

INSERT INTO `comment` (`id_comment`, `id_status`, `isi_comment`, `tgl`) VALUES
(2, 2, 'test komen', '2018-12-08 22:40:24'),
(3, 1, 'cek', '2018-12-08 23:28:34'),
(5, 1, 'cek', '2018-12-08 23:38:50'),
(6, 6, 'kljk', '2018-12-09 14:40:11'),
(7, 4, 'dsgsd', '2018-12-09 14:40:26'),
(8, 2, 'tes', '2018-12-09 14:40:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` enum('online','offline') NOT NULL,
  `last_aktive` datetime NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `status`, `last_aktive`, `nama`, `alamat`) VALUES
(5, 'krismon', '$2y$10$aYqCcGbXSGTJB4ioQRno.el1WdReB8Z81RrEuta60Cl5lMWZcEEW2', 'offline', '2018-12-09 14:43:08', 'Krismon', 'Jl. Raya Semanas'),
(6, 'qamarul', '$2y$10$cBa8Nba5F.p3Oc1Gd3mMFOEkemTsRpW3KpRtCRLUzj6TiDzWpRguG', 'offline', '2018-12-09 00:18:55', 'Qamarul Hazimin', 'Ketapang'),
(7, 'kevin', '$2y$10$3YV/W1KzYaNzVDvo/.OarOoweE6U2gc3fW/CGKb4NuUr34GbBjd/y', 'offline', '2018-12-09 02:04:59', 'Kevin Albert Setiawan', 'Pontianak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status`
--

CREATE TABLE `status` (
  `id_status` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `isi_status` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status`
--

INSERT INTO `status` (`id_status`, `id_user`, `isi_status`, `tanggal`) VALUES
(1, 5, 'Test', '2018-12-08 16:59:12'),
(2, 6, 'qamarul', '2018-12-08 21:16:33'),
(3, 7, 'Status kevin', '2018-12-08 22:07:09'),
(4, 7, 'mnggg', '2018-12-08 23:30:48'),
(5, 5, 'Buang\r\n\r\nIngin kubuang kemampuanku merangkai aksara\r\nAgar kubisa mengungkapkan rasa\r\nKerana memikirkan nona membuatku nestapa\r\nYang membuatku kian terlena\r\n\r\nSebab kata mereka aku tak pantas untuknya\r\nKata mereka aku sang petaka\r\nYang hanya bisa membawa bencana\r\nAku juga sang durjana yang tenggelam dalam nestapa\r\n\r\nAku tak peduli dengan kata mereka\r\nKarena kuanggap diriku adalah arjuna\r\nSeorang arjuna yang mencari cinta\r\nCinta dari nona EMILIA\r\n\r\nDuhai nona\r\n\r\nYang lebih indah dari andromeda\r\nTerangmu meredupkan bintang capella\r\nMengalahkan pesona lunar di angkasa\r\nBahkan aurora bukanlah tandingannya\r\n\r\nDuhai nona \r\n\r\nUbahlah aku yang disebut petaka\r\nUbahlah aku yang disebut durjana\r\nMenjadi aku yang kan disebut arjuna\r\nKarena mendapatkan cinta dari nona\r\n\r\nJika kudapatkan cinta dari nona\r\n\r\nKan kuubah pandangan mereka\r\nKan kubungkam mulut mereka\r\nSebab kini aku berbeda\r\nBukan petaka ataupun durjana\r\n\r\nAku bukan lagi sang petaka\r\nJuga aku bukan lagi seorang durjana\r\nKarena kini aku sang arjuna\r\nYang telah mendapatkan cinta nona EMILIA\r\n\r\n\r\n\r\nIlyas \r\nPontianak, 9 Desember 2018', '2018-12-09 02:33:13'),
(6, 5, 'Ingin kubuang kemampuanku merangkai aksara, Agar kubisa mengungkapkan semua rasa, Kerana memikirkan nona membuatku nestapa, Yang membuatku jadi sangat terlena.. Kata mereka aku tak pantas untuknya, Kata mereka aku adalah petaka, Yang hanya bisa membawa bencana, Kata mereka aku juga durjana yang tenggelam dalam nestapa.. Aku tak peduli dengan kata mereka, Karena kuanggap diriku adalah arjuna, Seorang arjuna yang sedang mencari cinta, Cinta dari nona EMILIA.. Duhai nona EMILIA... Yang lebih indah dari andromeda, Terangmu meredupkan bintang capella, Mengalahkan pesona lunar di angkasa, Bahkan aurora bukanlah tandingannya.. Duhai nona EMILIA... Ubahlah aku yang disebut petaka, Ubahlah aku yang disebut durjana, Menjadi aku yang kan disebut arjuna, Karena mendapatkan cinta dari nona.. Jika kudapatkan cinta nona EMILIA... Kan kuubah pandangan mereka, Kan kubungkam mulut mereka semua, Sebab kini aku berbeda.. Bukan petaka ataupun durjana, Aku tak bisa membawa bencana, Juga tak tenggelam dalam nestapa, Karena kini aku sang arjuna, Yang telah mendapatkan cinta nona EMILIA....... Ilyas. Pontianak, 9 Desember 2018', '2018-12-09 02:40:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `teman`
--

CREATE TABLE `teman` (
  `id_akun` int(11) NOT NULL,
  `id_teman` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `teman`
--

INSERT INTO `teman` (`id_akun`, `id_teman`) VALUES
(7, 6),
(6, 7),
(5, 6),
(6, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id_comment`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
